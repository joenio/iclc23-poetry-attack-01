**DEPRECATED** repository moved to https://codeberg.org/joenio/poetryattack

# Poetry Attack 01

Live Coding and Art Performance presented during ICLC 2023 by Mari Moura and
Joenio M Costa.

## Abstract

How many women are here? How many black people are here? How many black women
are here? In these times of displacement, it is necessary to think about it. In
this performance, the artists Mari Moura and Joenio M. Costa carries out a
poetry attack using live coding teachniques, text writing and body movements to
questioning the absence of women, black people, and black women at art and
technology spaces. The absence of women, black people and black women is part
of a systemic racist system that promotes exclusion. The performance act is a
call to reflect on the roles and opportunities to change it

The performance is composed by sound art, performance art, synthesizers, sound
samples and live coding using free software tools, like Sonic Pi, TidalCycles,
Super Collider, Le Biniou, PureData, dublang, OBS and other tools, including
not only live coding tools but any tool available via textual command line
interfaces. Live coding is a practice of artistic creation and a community for
building and using technology based on new non-utilitarian arrangements for
producing technology. The practice of live coding can be seen as a way of
creating sound and/or visual art using programming languages.

Mari Moura and Joenio M Costa have been experimenting live coding + body
performance for awhile now and they reached a maturity level where there is a
kind of communication during the live coding performances to suggests body
movements to be improvised along of the live performance.

### Space and equipment required

Laptop with internet connection, hdmi and p2 cables, sound system, 2
projectors, work table, chair, internet connection and microphones. The space
should have wi-fi for online connection, 2 projectors or 2 screens for
projecting laptop images. Room or hall with free space for body movements.

## Requirements

- Dublang
- SuperCollider
- Tidal Cycles

## Quickstart

```sh
make start
```

## ICLC Photos and Videos

- https://www.flickr.com/photos/creativecodingutrecht/52861879480/in/album-72177720307924791

## Authors

- Mari Moura <marimoura5@hotmail.com>
- Joenio Marques da Costa <joenio@joenio.me>
